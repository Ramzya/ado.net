using System;
using System.Data.SqlClient;

class Program
{
    static void Main(string[] args)
    {
        string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Paintings;Integrated Security=True";
        string sqlExpression1 = "INSERT INTO Master_alt (id, MasresName,DateofBirth) VALUES (77, 'Paolo','adad')";
        string sqlExpression2 = "DELETE FROM Master_alt";
        string sqlExpression3 = "UPDATE Master_alt SET MasresName='Jackil' WHERE MasresName='Paolo'";
        string sqlExpression4 = "SELECT * FROM Painting JOIN OrderDetails ON Painting.PaintingId = OrderDetails.PaintingId";
       

        Console.WriteLine("Input comand");
        string c = Console.ReadLine();
        switch (c)
        {
            case "Add":
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpression1, connection);
                    int number = command.ExecuteNonQuery();
                    Console.WriteLine("Item added ", number);

                }

                break;
            case "Delete":
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open(); 
                    SqlCommand command = new SqlCommand(sqlExpression2, connection);
                    int number = command.ExecuteNonQuery();
                    
                }
                break;
            case "Update":
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open(); 
                    SqlCommand command = new SqlCommand(sqlExpression3, connection);
                    int number = command.ExecuteNonQuery();
                    Console.WriteLine("????????? ????????: {0}", number);
                }
                break;
            case "Select":
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpression4, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows) 
                    {
                     
                        Console.WriteLine("{0} \t\t{1} \t\t{2} \t\t{3} \t\t{4} \t\t{5} \t\t{6} \t\t{7} \t\t{8}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4), reader.GetName(5), reader.GetName(6), reader.GetName(7), reader.GetName(8));

                        while (reader.Read()) 
                        {
                            object id = reader.GetValue(0);
                            object name = reader.GetValue(1);
                            object style = reader.GetValue(2);
                            object date = reader.GetValue(3);
                            object mID = reader.GetValue(4);
                            object eID = reader.GetValue(5);
                            object ON = reader.GetValue(6);
                            object pID = reader.GetValue(7);
                            object EID = reader.GetValue(8);

                            Console.WriteLine("{0} \t\t{1} \t\t{2} \t\t{3} \t\t{4} \t\t{5} \t\t{6} \t\t{7} \t\t{8} ", id, name, style, date, mID, eID, ON, pID, EID);
                        }
                    }
                    reader.Close();
                    break;
                    
                }
                


                }


                Console.Read();
        }
    }
